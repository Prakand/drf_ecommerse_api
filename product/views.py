from django.shortcuts import render
from .serializers import ProductSerializer, CategorySerializer, BrandSerializer
from .models import Product, Category, Brand
from rest_framework import viewsets
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema


@extend_schema(responses=CategorySerializer)
class CategoryViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for all category view
    """
    queryset = Category.objects.all()

    def list(self, request):
        serializer = CategorySerializer(self.queryset, many=True)
        return Response(serializer.data)


@extend_schema(responses=CategorySerializer)
class BrandViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for all Brand
    """
    queryset = Brand.objects.all()

    def list(self, request):
        serializer = BrandSerializer(self.queryset, many=True)
        return Response(serializer.data)
